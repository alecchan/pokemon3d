﻿var HealthBar : Transform;
var MaxHealth : float;
var MaxBar : float;
var Health : float;
var m_Camera : Camera;
 
 
function Update()
{
 
    // --- Change Size Of Bar --- \\
    transform.localScale.z = (MaxBar) * (Health/MaxHealth);
 
    // --- Change Colour Of Bar --- \\
    if(transform.localScale.z >= 2.6)
    {
        transform.renderer.material.color = Color.green;
    }
    if(transform.localScale.z < 2.6)
    {
        transform.renderer.material.color = Color.yellow;
    }
    if(transform.localScale.z <= 1.0)
    {
        transform.renderer.material.color = Color.red;
    }
}